### M-KOPA Android test

- The UI is delivered as per the wireframes
- A full suite of unit tests can be found in androidTest/
- As per the spec, changing the timer involves editing `ActiveUsagePeriodDataSource`
- As per the spec, changing the county involves editing `CountryIsoCodeDataSource`
