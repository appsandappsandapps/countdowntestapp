package com.example.mkopa

import com.example.mkopa.countdown.CoroutineCounter
import com.example.mkopa.countdown.CountdownRepository
import com.example.mkopa.countdown.Counter
import com.example.mkopa.datasources.ActiveUsagePeriodDataSource
import com.example.mkopa.datasources.ActiveUsagePeriodDataSourceImpl
import com.example.mkopa.datasources.CountryIsoCodeDataSource
import com.example.mkopa.datasources.CountryIsoCodeDataSourceImpl
import com.example.mkopa.datasources.CountryWarningTimeDataSource
import com.example.mkopa.datasources.CountryWarningTimeDataSourceImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

object ServiceLocator {

  lateinit var countdownRepository: CountdownRepository
    private set

  fun initialise() {
    val uiScope = CoroutineScope(Dispatchers.Main)
    countdownRepository = CountdownRepository(
      activeUsagePeriodDataSource = ActiveUsagePeriodDataSourceImpl(),
      countryCodeDataSource = CountryIsoCodeDataSourceImpl(),
      countryWarningTimeDataSource = CountryWarningTimeDataSourceImpl(),
      counter = CoroutineCounter(uiScope),
      counterDispatcher = Dispatchers.Default,
      ioDispatcher = Dispatchers.IO
    )
  }

}