package com.example.mkopa

import android.app.Application
import android.content.res.Configuration

class Application: Application() {

  override fun onCreate() {
    super.onCreate()
    ServiceLocator.initialise()
  }


}