package com.example.mkopa

import java.time.OffsetDateTime

val ZERO_TIME = OffsetDateTime.now()
  .withHour(0)
  .withMinute(0)
  .withSecond(0)
  .withNano(0)
