package com.example.mkopa.datasources

import kotlinx.coroutines.delay
import java.time.OffsetDateTime

data class ActiveUsagePeriod(
  val lockTime: OffsetDateTime,
)

interface ActiveUsagePeriodDataSource {
  suspend fun getLockingInfo(): ActiveUsagePeriod
}

// Hard coded as per the spec
class ActiveUsagePeriodDataSourceImpl : ActiveUsagePeriodDataSource {
  override suspend fun getLockingInfo(): ActiveUsagePeriod {
    delay(500)
    return ActiveUsagePeriod(
      OffsetDateTime.now().withHour(23).withMinute(0).withSecond(0).withNano(0)
    )
  }
}