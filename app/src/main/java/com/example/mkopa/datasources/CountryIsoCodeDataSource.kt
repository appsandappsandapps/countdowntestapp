package com.example.mkopa.datasources

import com.example.mkopa.data.CountryIsoCode

interface CountryIsoCodeDataSource {
    suspend fun getCountryIsoCode(): CountryIsoCode
}

// Hard coded as per the spec
class CountryIsoCodeDataSourceImpl : CountryIsoCodeDataSource {
    override suspend fun getCountryIsoCode(): CountryIsoCode {
        return CountryIsoCode.KE
    }
}