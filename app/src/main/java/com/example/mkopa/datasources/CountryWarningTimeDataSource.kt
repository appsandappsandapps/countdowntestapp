package com.example.mkopa.datasources

import com.example.mkopa.ZERO_TIME
import com.example.mkopa.data.CountryIsoCode
import java.time.OffsetDateTime

// Hard coded but would be better to come
// from a service, a implementation of the interface
// below could deal with that
private val countriesWarningTimes = hashMapOf(
    CountryIsoCode.UG to OffsetDateTime.now().withHour(3).withMinute(0).withSecond(0).withNano(0),
    CountryIsoCode.KE to OffsetDateTime.now().withHour(2).withMinute(0).withSecond(0).withNano(0)
)

interface CountryWarningTimeDataSource {
    suspend fun getWarningTime(code: CountryIsoCode): OffsetDateTime
}

// Hard coded for testing purposes
class CountryWarningTimeDataSourceImpl : CountryWarningTimeDataSource {

    private val defaultWarningTime = ZERO_TIME
    override suspend fun getWarningTime(code: CountryIsoCode): OffsetDateTime {
        val time = countriesWarningTimes[code]
        return if (time != null) time else defaultWarningTime
    }

}