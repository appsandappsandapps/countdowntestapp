package com.example.mkopa.data

enum class CountdownColour {
  GREEN,
  WARNING,
  RED
}