package com.example.mkopa.data

// Hard coded as per the spec
enum class CountryIsoCode {
    UG,
    KE
}