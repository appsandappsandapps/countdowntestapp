package com.example.mkopa.countdown

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mkopa.ServiceLocator
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * - Starts the countdown via the repository
 * - Passes updates to the time and indication warning / colour
 *   to the uiModel
 */
class CountdownViewModel(
  private val countdownRepository: CountdownRepository = ServiceLocator.countdownRepository,
  private val uiModelMock: CountdownUIModel? = null,
) : ViewModel() {

  val uiModel = uiModelMock ?: CountdownUIModel()

  init {
    viewModelScope.launch {
      countdownRepository.countdownTimer.collect {
        uiModel.setCountdownTime(it)
      }
    }
    viewModelScope.launch {
      countdownRepository.countdownIndicatorColour.collect {
        uiModel.setCountdownIndicatorColour(it)
      }
    }
    countdownRepository.getUsagePeriodAndCountdown()
  }

}