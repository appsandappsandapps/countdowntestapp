package com.example.mkopa.countdown

import com.example.mkopa.ZERO_TIME
import com.example.mkopa.data.CountdownColour
import com.example.mkopa.datasources.ActiveUsagePeriodDataSource
import com.example.mkopa.datasources.CountryIsoCodeDataSource
import com.example.mkopa.datasources.CountryWarningTimeDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.time.OffsetDateTime

/**
 * Fetches the lock time, warning time, and starts a
 * counter, updating some MutableState to show changes
 * in the time and indicator/warning colour
 *
 * - Holds the current countdown timer value
 * - Holds the current indicator/warning colour
 */
class CountdownRepository(
  private val activeUsagePeriodDataSource: ActiveUsagePeriodDataSource,
  private val countryCodeDataSource: CountryIsoCodeDataSource,
  private val countryWarningTimeDataSource: CountryWarningTimeDataSource,
  private val counter: Counter,
  private val counterDispatcher: CoroutineDispatcher,
  private val ioDispatcher: CoroutineDispatcher,
) {

  var currentCountdown: Job? = null

  var countdownTimer = MutableStateFlow(ZERO_TIME)
    private set

  var countdownIndicatorColour = MutableStateFlow(CountdownColour.GREEN)
    private set

  private var countdownWarningTime = ZERO_TIME

  /**
   * - Sets the warning time and lock time
   * - Starts the countdown
   *
   * Started in the io dispatcher to get the info
   * the passed to the counter dispatcher to process
   * the countdown
   */
  fun getUsagePeriodAndCountdown() {
    CoroutineScope(ioDispatcher).launch {
      getCountryWarningTime()
      val lockTime = activeUsagePeriodDataSource.getLockingInfo().lockTime
      setNewTimeAndIndicatorColour(lockTime, countdownWarningTime)
      currentCountdown?.cancel()
      currentCountdown = CoroutineScope(counterDispatcher).launch {
        startCountdown(lockTime)
      }
    }
  }

  private suspend fun getCountryWarningTime() {
    val code = countryCodeDataSource.getCountryIsoCode()
    val time = countryWarningTimeDataSource.getWarningTime(code)
    countdownWarningTime = time
  }

  /**
   * Minuses the a second from countdownTimer
   * and then sets the new time
   */
  private suspend fun startCountdown(startTime: OffsetDateTime) {
    val totalSeconds = startTime.second + (startTime.minute * 60) + (startTime.hour * 60 * 60)
    counter.start(
      totalSeconds.toLong(),
      {
        val newTime = countdownTimer.value.minusSeconds(1)
        setNewTimeAndIndicatorColour(newTime, countdownWarningTime)
      }
    )
  }

  private fun setNewTimeAndIndicatorColour (
    time: OffsetDateTime,
    warningTime: OffsetDateTime,
  ) {
    val comparedWarningTime = time.compareTo(warningTime)
    if (time == ZERO_TIME) {
      countdownIndicatorColour.value = CountdownColour.RED
    } else if (comparedWarningTime < 0 || comparedWarningTime == 0) {
      countdownIndicatorColour.value = CountdownColour.WARNING
    } else {
      countdownIndicatorColour.value = CountdownColour.GREEN
    }
    countdownTimer.value = time
  }

}