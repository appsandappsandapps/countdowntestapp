package com.example.mkopa.countdown

import android.os.CountDownTimer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

interface Counter {
  suspend fun start(
    totalSeconds: Long,
    callback: () -> Unit
  )
}

/**
 * Counts down from a specified point in start()
 * and calls the callback from with in the collection scope.
 *
 * Should be called from within a Dispatchers.Default for
 * best results
 */
class CoroutineCounter(
  val collectionScope: CoroutineScope,
): Counter {
  override suspend fun start(
    totalSeconds: Long,
    callback: () -> Unit
  ) {
    (1..totalSeconds)
      .asSequence()
      .asFlow()
      .onEach {
        delay(1_000)
      }
      .collect {
        collectionScope.launch {
          callback()
        }
      }
  }
}