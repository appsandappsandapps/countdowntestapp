package com.example.mkopa.countdown

import com.example.mkopa.ZERO_TIME
import com.example.mkopa.data.CountdownColour
import kotlinx.coroutines.flow.MutableStateFlow
import java.time.OffsetDateTime

data class CountdownUIState(
  val countdownTime: OffsetDateTime = ZERO_TIME,
  val indicatorColour: CountdownColour = CountdownColour.GREEN
)

class CountdownUIModel() {

  val uiState = MutableStateFlow(CountdownUIState())

  fun setCountdownTime(time: OffsetDateTime) {
    uiState.value = uiState.value.copy(
      countdownTime = time,
    )
  }

  fun setCountdownIndicatorColour(colour: CountdownColour) {
    uiState.value = uiState.value.copy(
      indicatorColour = colour
    )
  }

}