package com.example.mkopa.countdown

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.mkopa.data.CountdownColour
import java.time.OffsetDateTime

@Composable
fun CountdownUI() {
  val viewModel: CountdownViewModel = viewModel()
  val uiModel = viewModel.uiModel
  val uiState by uiModel.uiState.collectAsState()

  CountdownContent(
    uiState.countdownTime,
    uiState.indicatorColour
  )
}

@Composable
private fun CountdownContent(
  time: OffsetDateTime,
  colourEnum: CountdownColour
) {
  val colour = when(colourEnum) {
    CountdownColour.GREEN -> Color.Green
    CountdownColour.RED -> Color.Red
    CountdownColour.WARNING -> Color(0xFFFFA500)
  }
  val hour = time.hour.toString().padStart(2, '0')
  val min = time.minute.toString().padStart(2, '0')
  val sec = time.second.toString().padStart(2, '0')
  val timeString = "$hour:$min:$sec"
  Box(
    Modifier
      .fillMaxSize()
  ) {
    Box(
      Modifier
        .align(Alignment.Center)
        .fillMaxWidth(0.8f)
        .border(
          BorderStroke(2.dp, Color.Black),
          RoundedCornerShape(20.dp)
        )
        .background(
          colour,
          RoundedCornerShape(20.dp),
        ),
    ) {
      Text(
        timeString,
        Modifier
          .padding(vertical = 50.dp)
          .align(Alignment.Center),
        fontSize = 30.sp
      )
    }
  }
}
