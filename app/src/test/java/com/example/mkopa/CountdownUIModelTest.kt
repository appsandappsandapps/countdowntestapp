package com.example.mkopa

import com.example.mkopa.countdown.CountdownUIModel
import com.example.mkopa.data.CountdownColour
import kotlinx.coroutines.test.runTest
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.time.OffsetDateTime

@RunWith(MockitoJUnitRunner::class)
class CountdownUIModelTest {

  @Test
  fun `green indicator on init`() = runTest {
    val uiModel = CountdownUIModel()
    val uiState = uiModel.uiState.value

    assertEquals(
      uiState.indicatorColour,
      CountdownColour.GREEN
    )
  }

  @Test
  fun `zero time on init`() = runTest {
    val uiModel = CountdownUIModel()
    val uiState = uiModel.uiState.value

    assertEquals(
      uiState.countdownTime,
      ZERO_TIME
    )
  }

  @Test
  fun `new set time reflected`() = runTest {
    val uiModel = CountdownUIModel()
    val newTime = OffsetDateTime.now()
    uiModel.setCountdownTime(newTime)

    val uiState = uiModel.uiState.value
    assertEquals(
      uiState.countdownTime,
      newTime
    )
  }

  @Test
  fun `new colour time reflected`() = runTest {
    val uiModel = CountdownUIModel()
    uiModel.setCountdownIndicatorColour(CountdownColour.RED)

    val uiState = uiModel.uiState.value
    assertEquals(
      uiState.indicatorColour,
      CountdownColour.RED
    )
  }

}