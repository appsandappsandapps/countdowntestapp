package com.example.mkopa

import com.example.mkopa.countdown.CountdownRepository
import com.example.mkopa.countdown.CountdownUIModel
import com.example.mkopa.countdown.CountdownViewModel
import com.example.mkopa.data.CountdownColour
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.time.OffsetDateTime

@RunWith(MockitoJUnitRunner::class)
class CountdownViewModelTest {

  @Mock
  lateinit var countdownRepository: CountdownRepository
  @Mock
  lateinit var countdownUIModel: CountdownUIModel

  @Before
  fun setUp() {
    Dispatchers.setMain(StandardTestDispatcher())
  }

  @Test
  fun `fetch lock period and countdown`() = runTest {

    `when`(countdownRepository.countdownTimer)
      .thenReturn(MutableStateFlow(OffsetDateTime.now()))
    `when`(countdownRepository.countdownIndicatorColour)
      .thenReturn(MutableStateFlow(CountdownColour.GREEN))

    val countdownViewModel =
      CountdownViewModel(countdownRepository)

    verify(countdownRepository, times(1))
      .getUsagePeriodAndCountdown()
  }

  @Test
  fun `pass new date time to uimodel`() = runTest {

    val offsetDateTimeFlow = MutableStateFlow<OffsetDateTime>(OffsetDateTime.now())
    `when`(countdownRepository.countdownTimer)
      .thenReturn(offsetDateTimeFlow)
    `when`(countdownRepository.countdownIndicatorColour)
      .thenReturn(MutableStateFlow(CountdownColour.GREEN))

    launch {
      val countdownViewModel =
        CountdownViewModel(
          countdownRepository,
          countdownUIModel
        )
      offsetDateTimeFlow.value = OffsetDateTime.now()
    }.join()

    verify(countdownUIModel, times(1))
      .setCountdownTime(offsetDateTimeFlow.value)
  }

  @Test
  fun `pass new indicator warning colour to uimodel`() = runTest {

    val countdownIndicatorColourFlow = MutableStateFlow(CountdownColour.GREEN)
    `when`(countdownRepository.countdownTimer)
      .thenReturn(MutableStateFlow(OffsetDateTime.now()))
    `when`(countdownRepository.countdownIndicatorColour)
      .thenReturn(countdownIndicatorColourFlow)

    launch {
      val countdownViewModel =
        CountdownViewModel(
          countdownRepository,
          countdownUIModel
        )
      countdownIndicatorColourFlow.value = CountdownColour.WARNING
    }.join()

    verify(countdownUIModel, times(1))
      .setCountdownIndicatorColour(CountdownColour.WARNING)
  }

}