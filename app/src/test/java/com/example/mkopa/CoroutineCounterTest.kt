package com.example.mkopa

import com.example.mkopa.countdown.CoroutineCounter
import com.example.mkopa.countdown.CountdownUIModel
import com.example.mkopa.data.CountdownColour
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.time.OffsetDateTime
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class CoroutineCounterTest {

  @Mock
  lateinit var tickFun: () -> Unit

  val testDispatcher: CoroutineDispatcher = StandardTestDispatcher()

  @Before
  fun setUp() {
    Dispatchers.setMain(testDispatcher)
  }

  @Test
  fun `counter counts once if passed 1`() = runTest {
    val counter = CoroutineCounter(CoroutineScope(testDispatcher))

    launch {
      counter.start(1, tickFun)
    }.join()

    verify(tickFun, times(1)).invoke()
  }

  @Test
  fun `counter counts zero times if passed 0`() = runTest {
    val counter = CoroutineCounter(CoroutineScope(testDispatcher))

    launch {
      counter.start(0, tickFun)
    }.join()

    verify(tickFun, times(0)).invoke()
  }

  @Test
  fun `counter has a second interval`() = runTest {
    val counter = CoroutineCounter(CoroutineScope(testDispatcher))

    var firstDateTime: Long? = null
    var secondDateTime: Long? = null

    launch(newSingleThreadContext("new test thread")) {
      counter.start(2, {
        if(firstDateTime == null) {
          firstDateTime = Date().time
        } else {
          secondDateTime = Date().time
        }
      })
    }.join()

    val firstTick = firstDateTime!! / 1000
    val secondTick = secondDateTime!! / 1000

    Assert.assertEquals(
      firstTick + 1,
      secondTick
    )
  }

}