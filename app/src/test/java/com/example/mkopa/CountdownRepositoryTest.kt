package com.example.mkopa

import com.example.mkopa.countdown.CountdownRepository
import com.example.mkopa.countdown.Counter
import com.example.mkopa.data.CountdownColour
import com.example.mkopa.datasources.ActiveUsagePeriod
import com.example.mkopa.datasources.ActiveUsagePeriodDataSource
import com.example.mkopa.data.CountryIsoCode
import com.example.mkopa.datasources.CountryIsoCodeDataSource
import com.example.mkopa.datasources.CountryWarningTimeDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.time.OffsetDateTime

@RunWith(MockitoJUnitRunner::class)
class CountdownRepositoryTest {

  @Mock
  lateinit var activeUsagePeriodDataSource: ActiveUsagePeriodDataSource
  @Mock
  lateinit var countryCodeDataSource: CountryIsoCodeDataSource
  @Mock
  lateinit var countryWarningTimeDataSource: CountryWarningTimeDataSource
  @Mock
  lateinit var counter: Counter
  val testDispatcher: CoroutineDispatcher = StandardTestDispatcher()

  @Before
  fun setUp() {
    Dispatchers.setMain(testDispatcher)
  }

  @Test
  fun `countdown warning color amber when warning and lock time the same`() = runTest {

    var countdownRepository: CountdownRepository? = null
    val countdownRemainingTime = OffsetDateTime.now()
    val warningTime = countdownRemainingTime

    setupCountryCodeAndWarningTimeAndLockingTime(
      warningTime,
      countdownRemainingTime
    )

    launch {
      countdownRepository = CountdownRepository(
        activeUsagePeriodDataSource,
        countryCodeDataSource,
        countryWarningTimeDataSource,
        counter,
        testDispatcher,
        testDispatcher,
      )
      countdownRepository!!.getUsagePeriodAndCountdown()
    }.join()

    verify(countryCodeDataSource, times(1))
      .getCountryIsoCode()

    verify(countryWarningTimeDataSource, times(1))
      .getWarningTime(CountryIsoCode.KE)

    Assert.assertEquals(
      countdownRepository!!.countdownIndicatorColour.value,
      CountdownColour.WARNING
    )

  }

  @Test
  fun `countdown warning color red when warning and lock time the same`() = runTest {

    var countdownRepository: CountdownRepository? = null
    val countdownRemainingTime = ZERO_TIME
    val warningTime = OffsetDateTime.now()

    setupCountryCodeAndWarningTimeAndLockingTime(
      warningTime,
      countdownRemainingTime
    )

    launch {
      countdownRepository = CountdownRepository(
        activeUsagePeriodDataSource,
        countryCodeDataSource,
        countryWarningTimeDataSource,
        counter,
        testDispatcher,
        testDispatcher,
      )
      countdownRepository!!.getUsagePeriodAndCountdown()
    }.join()

    verify(countryCodeDataSource, times(1))
      .getCountryIsoCode()

    verify(countryWarningTimeDataSource, times(1))
      .getWarningTime(CountryIsoCode.KE)

    Assert.assertEquals(
      countdownRepository!!.countdownIndicatorColour.value,
      CountdownColour.RED
    )

  }

  @Test
  fun `countdown warning color green when not passed warning time`() = runTest {

    var countdownRepository: CountdownRepository? = null
    val countdownRemainingTime = OffsetDateTime.now()
    val warningTime = ZERO_TIME

    setupCountryCodeAndWarningTimeAndLockingTime(
      warningTime,
      countdownRemainingTime
    )

    launch {
      countdownRepository = CountdownRepository(
        activeUsagePeriodDataSource,
        countryCodeDataSource,
        countryWarningTimeDataSource,
        counter,
        testDispatcher,
        testDispatcher,
      )
      countdownRepository!!.getUsagePeriodAndCountdown()
    }.join()

    verify(countryCodeDataSource, times(1))
      .getCountryIsoCode()

    verify(countryWarningTimeDataSource, times(1))
      .getWarningTime(CountryIsoCode.KE)

    Assert.assertEquals(
      countdownRepository!!.countdownIndicatorColour.value,
      CountdownColour.GREEN
    )

  }

  @Test
  fun `countdown time set from datasource`() = runTest {

    var countdownRepository: CountdownRepository? = null
    val countdownRemainingTime = OffsetDateTime.now()
    val warningTime = ZERO_TIME

    setupCountryCodeAndWarningTimeAndLockingTime(warningTime, countdownRemainingTime)

    launch {
      countdownRepository = CountdownRepository(
        activeUsagePeriodDataSource,
        countryCodeDataSource,
        countryWarningTimeDataSource,
        counter,
        testDispatcher,
        testDispatcher,
      )
      countdownRepository!!.getUsagePeriodAndCountdown()
    }.join()

    Assert.assertEquals(
      countdownRepository!!.countdownTimer.value,
      countdownRemainingTime
    )

  }

  @Test
  fun `countdown time minused via Counter`() = runTest {

    var countdownRepository: CountdownRepository? = null
    val countdownRemainingTime = OffsetDateTime.now()
    val countdownRemainingTimeAfterMinus = countdownRemainingTime.minusSeconds(1)
    val warningTime = ZERO_TIME

    setupCountryCodeAndWarningTimeAndLockingTime(
      warningTime,
      countdownRemainingTime
    )

    // Make it tick once
    val counter = object : Counter {
      override suspend fun start(totalSeconds: Long, callback: () -> Unit) {
        callback()
      }
    }

    launch {
      countdownRepository = CountdownRepository(
        activeUsagePeriodDataSource,
        countryCodeDataSource,
        countryWarningTimeDataSource,
        counter,
        testDispatcher,
        testDispatcher,
      )
      countdownRepository!!.getUsagePeriodAndCountdown()

      countdownRepository!!.countdownTimer
        .take(3).collectIndexed { i, time ->
          println("$i $time")
          when(i) {
            0 ->
              Assert.assertEquals(
                ZERO_TIME,
                time
              )
            1 ->
              Assert.assertEquals(
                countdownRemainingTime,
                time
              )
            2 ->
              Assert.assertEquals(
                countdownRemainingTimeAfterMinus,
                time
              )
          }
        }
    }.join()

  }

  private suspend fun setupCountryCodeAndWarningTimeAndLockingTime(
    warningTime: OffsetDateTime?,
    countdownRemainingTime: OffsetDateTime
  ) {
    `when`(countryCodeDataSource.getCountryIsoCode())
      .thenReturn(CountryIsoCode.KE)

    `when`(countryWarningTimeDataSource.getWarningTime(CountryIsoCode.KE))
      .thenReturn(warningTime)

    `when`(activeUsagePeriodDataSource.getLockingInfo())
      .thenReturn(ActiveUsagePeriod(countdownRemainingTime))
  }

}